package com.jsh.erp.constants;

/**
 * @ClassName:BusinessConstants
 * @Description 业务字典类
 * @Author linshengming
 * @Date 2018-9-15 17:58
 * @Version 1.0
 **/
public class BusinessConstants {

    /**
     * 默认的日期格式
     */
    public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * 默认的分页起始页页码
     */
    public static final String DEFAULT_PAGINATION_PAGE_NUMBER = "1";
    /**
     * 默认的分页页数
     */
    public static final String DEFAULT_PAGINATION_PAGE_SIZE = "10";
    /**
     * 商品是否开启序列号标识enableSerialNumber 0否false，1是true
     *
     * */
    public static final boolean MATERIAL_ENABLE_SERIAL_NUMBER = true;
    public static final boolean MATERIAL_NOT_ENABLE_SERIAL_NUMBER = false;
    /**
     * 单据主表出入库类型 type 入库 出库
     * depothead
     * */
    public static final String DEPOTHEAD_TYPE_STORAGE = "入库";
    public static final String DEPOTHEAD_TYPE_OUT = "出库";







}
